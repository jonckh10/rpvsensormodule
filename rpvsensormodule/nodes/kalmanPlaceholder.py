#!/usr/bin/env python
import roslib; roslib.load_manifest('rpvsensormodule')
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import NavSatFix, Imu
from rpvsensormodule.msg import kalmanOut
from rospy.rostime import Duration
from rospy.timer import Timer
import math

class KalmanPlaceholder:

    def __init__(self): 
        # direction that the uav is pointing
        # 0,0,0 is east
        self.baseLat = None
        self.baseLon = None
        
        self.position = None
        self.direction = None
        self.velocity = None

        self.metersPerLat = None
        self.metersPerLon = None


    def computeMetersPerLatLon(self, lat, lon): 
        self.baseLat = lat
        self.baseLon = lon

        # equatorial radius
        a = 6378137.0
        PI = math.pi
        latRad = lat * PI / 180

        # eccentricity squared 
        e2 = 0.00669437999014

        # http://en.wikipedia.org/wiki/Lattitude#The_length_of_a_degree_of_latitude
        self.metersPerLat = PI * a * (1 - e2) / (180 * (1 - e2 * math.sin(latRad)**2)**1.5)

        # http://en.wikipedia.org/wiki/Longitude#Length_of_a_degree_of_longitude 
        self.metersPerLon = PI * a * math.cos(latRad) / (180 * (1 - e2 * math.sin(latRad)**2)**0.5)

    def publish(self, timerEvent = None):
        if self.direction == None:
            return None

        data = kalmanOut()
        data.sensorLatitude, data.sensorLongitude = self.metersToLatLon(self.position[0],self.position[1])
        data.sensorAltitude = self.position[2]

        rospy.loginfo("Out: Latitude: %s", data.sensorLatitude)
        rospy.loginfo("Out: Longitude: %s", data.sensorLongitude)
        rospy.loginfo("Out: Altitude: %s", data.sensorAltitude)

        self.pub.publish(data)
        return 1 

    def norm(self, vect3):
        magn = 0
        for x in vect3:
            magn += x*x 
        if magn == 0:
            return [0,0,0]
        magn **= 0.5
        for i in range(len(vect3)):
            vect3[i] /= magn
        return vect3

    def latLonToMeters(self, lat, lon):
        metersNorth = (lat - self.baseLat) * self.metersPerLat        
        metersEast = (lon - self.baseLon) * self.metersPerLon        
        return (metersNorth, metersEast)

    def metersToLatLon(self, metersNorth, metersEast):
        lat = metersNorth / self.metersPerLat + self.baseLat       
        lon = metersEast / self.metersPerLon + self.baseLon       

        return (lat, lon)


    def gpsCallback(self, data):

        if self.metersPerLat == None:
            self.computeMetersPerLatLon(data.latitude, data.longitude)
            self.position = [0,0,0]
            self.position[0], self.position[1] = self.latLonToMeters(data.latitude, data.longitude)
            self.position[2] = data.altitude
        else:
            self.direction = [0,0,0]
            oldPosition = list(self.position)
            self.position[0], self.position[1] = self.latLonToMeters(data.latitude, data.longitude)
            self.position[2] = data.altitude
            for i in range(3):
                self.direction[i] = self.position[i] - oldPosition[i]
            self.direction = self.norm(self.direction)

        rospy.loginfo("In: Latitude: %s", data.latitude)
        rospy.loginfo("In: Longitude: %s", data.longitude)
        rospy.loginfo("In: Altitude: %s", data.altitude)

    def imuCallback(self, data):
        self.pub.publish(gpsData)

    def listener(self): 
        rospy.init_node('kalPlace', anonymous=True)

        rospy.Subscriber("fix", NavSatFix, self.gpsCallback)
        rospy.Subscriber("imu", Imu, self.imuCallback)

        self.pub = rospy.Publisher('kalmanOut', kalmanOut)
        # self.timer = Timer(Duration(0,3333333), self.publish) #30 Hz
        self.timer = Timer(Duration(0,1000000000), self.publish) #1 Hz
        rospy.spin()

if __name__ == '__main__':
    k = KalmanPlaceholder()
    k.listener()

