#pragma once
#include <vector>
#include <inttypes.h>
#include <string>
#include <iostream>
#include "tlv.h"

using namespace std;

// This class is used to build a KLV packet. In its current form, it has several limitations:
//  * No internal Local Data Set (LDS)
//  * Ul Key is hardcoded
//  * Tags can only be one byte long
// Websites with more information on KLV: http://www.gwg.nga.mil/misb/docs/standards/Standard090201.pdf
//                                        http://www.gwg.nga.mil/misb/docs/standards/Standard060104.pdf   
class KlvFactory
{
private:
	int totalLen;
	vector<Tlv*> items;

	unsigned short bcc_16 ( unsigned char * buff, unsigned short len );

	void addInt(char tag, char len, uint64_t value);
public: 
	KlvFactory();
	
 
   void addInt8(char tag,  uint8_t value);

   void addInt16(char tag,  uint16_t value);

   void addInt32(char tag,  uint32_t value);

   void addInt64(char tag,  uint64_t value);
  
   void addStr(int tag, string s);

//TODO void addLds(char* lds, int len);

//TODO void setUlKey(char *key, int len);
	
	vector<unsigned char> finish();
};

