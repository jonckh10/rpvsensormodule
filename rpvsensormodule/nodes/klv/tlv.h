#pragma once

#include <vector>
#include <inttypes.h>
#include <string>

using namespace std;

// Abstract struct that defines the form of a Tag Length Value (TLV) item
// Websites with more information on KLV: http://www.gwg.nga.mil/misb/docs/standards/Standard090201.pdf
//                                        http://www.gwg.nga.mil/misb/docs/standards/Standard060104.pdf   

struct Tlv
{
public:

   //appends the hex representation of the item to out
	virtual void output(vector<unsigned char>& out) = 0;

   //computes the ber represenation of an length, and appends it to out
	static void berLen(vector<unsigned char>& out, int len)
	{
		if(len <= 127)
		{
			uint8_t lenByte = len;
			out.push_back(lenByte);
		}
		else
		{		
			out.push_back(0x80);
			int lenIndex = out.size() - 1;
			//get past empty high order bytes			
			int offset = (sizeof(len) - 1) * 8;

			while(((len >> offset) & 0xFF) == 0)
			{
				offset -= 8;
			}
			while(offset >= 0)
			{
				char byte = len >> offset;
				out.push_back(byte);
				out[lenIndex]++;
				offset -= 8;
			}
		}
	}
};

//TLV string item
struct TlvStr : public Tlv
{
   char tag; 
   std::string str;
   void output(vector<unsigned char>& out)
	{
		out.push_back(tag);
		Tlv::berLen(out, str.size());
		//size-1 is to skip \0
		for(unsigned int i = 0; i < str.size(); i++)
		{
			out.push_back(str.at(i));
		}
	}
};

//TLV integer item. Can hold 64 bits. 
struct TlvInt : public Tlv
{
	char tag;
	char len;
	uint64_t value; //integer value occupies the len bottom bytes of the value int

	void output(vector<unsigned char>& out)
	{
		out.push_back(tag);
		out.push_back(len);
		for(int i = len-1; i >= 0; i--)
		{
			out.push_back((value >> i*8) & 0xFF);
		}
	}

};
