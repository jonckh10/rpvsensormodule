#include <vector>
#include <inttypes.h>
#include <string>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "tlv.h"
#include "klvFactory.h"


//Computes 16 bit checksum of KLV packet
unsigned short KlvFactory::bcc_16 (
 unsigned char * buff, // Pointer to the first byte in the 16-byte UAS LDS key.
 unsigned short len ) // Length from 16-byte UDS key up to 1-byte checksum length.
{
	unsigned short bcc = 0, i; // Initialize Checksum and counter variables.
	for ( i = 0 ; i < len; i++)
		bcc += buff[i] << (8 * ((i + 1) % 2));
	return bcc;
} // end of bcc_16 ()


//constructor
KlvFactory::KlvFactory()
{
	totalLen = 0;
}

//adds a TLV integer item to the KLV packet 
//   char tag: tag of the TLV packet
//   char len: how long the integer is
//   uint64_t value: the integer itself. Occupies the bottom len bytes
void KlvFactory::addInt(char tag, char len, uint64_t value)
{
	TlvInt *item = new TlvInt;
	item->tag = tag;
	item->len = len;
	item->value = value;

	totalLen += len + 2;
	items.push_back(item);
}

void KlvFactory::addInt8(char tag,  uint8_t value)
{
   addInt(tag, 1, value);
}

void KlvFactory::addInt16(char tag,  uint16_t value)
{
   addInt(tag,2,value);
}

void KlvFactory::addInt32(char tag,  uint32_t value)
{
   addInt(tag,4,value);
}

void KlvFactory::addInt64(char tag,  uint64_t value)
{
   addInt(tag,8,value);
}


// Adds a TLV string item. Length is computed from the string
// int tag: tag of the TLV item
// string s: string value of the TLV item
void KlvFactory::addStr(int tag, string s)
{
    TlvStr *item = new TlvStr;
	item->tag = tag;
	item->str = s;

	//length of the beroid-encoded length field for the item
	int lenLen = s.size() < 127 ? 1 : 1 + s.size() / 256;
	totalLen += 1 + lenLen + s.size();
	items.push_back(item);	

}

//Did not have time to implement: adding and building local dataset. 
//void KlvFactory::addLds(char* lds, int len);

// do all final operations on the KLV packet and return its hex representation
vector<unsigned char> KlvFactory::finish()
{
	vector<unsigned char> out;
	
	char ulKey[] = { 0x06, 0x0E, 0x2B, 0x34, 0x02, 0x0B, 0x01, 0x01, 0x0E, 0x01, 0x03, 0x01, 0x01, 0x00, 0x00, 0x00 };
	
	//write ul key
	totalLen += sizeof(ulKey);
	for(int i = 0; i < sizeof(ulKey); i++)
	{
		out.push_back(ulKey[i]);
	}
	
	//write length field
	totalLen += 4; // add in space for checksum
	Tlv::berLen(out, totalLen);

	//write item values
	for(int i = 0; i < items.size(); i++)
	{
		items[i]->output(out);
	}

	//write checksum item
	out.push_back(1); //checksum tag
	out.push_back(2); //checksum length
    
    //compute checksum
	uint16_t checksum = bcc_16(out.data(), out.size());
	out.push_back(checksum >> 8);
	out.push_back(checksum & 0xFF);

	return out;
}
