#include "ros/ros.h"
#include "std_msgs/String.h"

#include "rpvsensormodule/kalmanOut.h"
#include <vector>
#include <inttypes.h>
#include <string>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "klvFactory.h"
#include "time.h"
#include <fstream>

fstream coorFile;
ros::Publisher klvPub ;

//only works up to int32
double doubleToRangeUnsigned(double x, double low, double high, uint intRange)
{
   double range = high - low;
   cout << "x: " << x << endl;
   cout << "Range: " << range << endl;
   cout << "intRange: " << hex << intRange << endl;
   double retVal = intRange / range * (x  - low);
   cout << "result in dec: " << dec << retVal << " vs " << 0xC221 << endl;

   return retVal;
}

//only works up to int32
//x: the value to convert
//maxX: maximum positive value for x. Assumes minimum negative is -maxX
//bitsInt: number of bits in the target integer type
double doubleToRangeSigned(double x, double maxX, uint intRange)
{
   uint32_t error = -intRange / 2 - 1 ; //error is most negative number

   if( x > maxX || x < -maxX)
   {
      cout << "ERROR" << endl;
      cout << x << ">" << maxX<< endl; 
      return error;
   }

   cout << x / maxX <<endl;
   cout << intRange << endl;
   cout << intRange/2 << endl;
   double retVal = x / maxX * (intRange / 2);
   cout << retVal << endl;
   return retVal;
}


uint16_t doubleToUint16(double x, double minX, double maxX)
{
   return static_cast<uint16_t>(doubleToRangeUnsigned(x, minX, maxX, 0xFFFF));
}

uint16_t doubleToInt16(double x, double maxX)
{
   return static_cast<int16_t>(doubleToRangeSigned(x, maxX, 0xFFFF));
}


uint32_t doubleToUint32(double x, double minX, double maxX)
{
   return static_cast<uint32_t>(doubleToRangeUnsigned(x, minX, maxX, 0xFFFFFFFF));
}

uint32_t doubleToInt32(double x, double maxX)
{
   return static_cast<int32_t>(doubleToRangeSigned(x, maxX, 0xFFFFFFFF));
}



void chatterCallback(const rpvsensormodule::kalmanOut::ConstPtr& msg)
{
   KlvFactory fact;
   //timestamp first
   uint64_t unixTime = time(NULL); //time since unix epoch in seconds
   unixTime *= 1000000; //time since unix epoch in microseconds
   fact.addInt64(2, unixTime);

   //latitude, longitude, altitude in WGS84 coordinates
   uint16_t altitude = doubleToUint16(msg->sensorAltitude, -900, 19000);
   fact.addInt16(75, altitude); //sensor ellipsoid height conversion

   uint32_t latitude = doubleToInt32(msg->sensorLatitude, 90); 
   fact.addInt32(13, latitude); //sensor latitude
 
   uint32_t longitude = doubleToInt32(msg->sensorLongitude, 180); 
   fact.addInt32(14, longitude); //sensor latitude   

   vector<unsigned char> output = fact.finish();

   //Output the KLV data to the serial node 

   std_msgs::String outMsg; 
   std::stringstream ss;

   for(unsigned int i = 0; i < output.size(); i++)
   {
      ss << output[i];
   } 
    
   outMsg.data = ss.str();

   klvPub.publish(outMsg);
   
}

int main(int argc, char **argv)
{


  ros::init(argc, argv, "klvNode");
  ros::NodeHandle n;

// %Tag(SUBSCRIBER)%
  ros::Subscriber sub = n.subscribe("kalmanOut", 1000, chatterCallback);
// %EndTag(SUBSCRIBER)%

   klvPub = n.advertise<std_msgs::String>("klvOut", 30);

// %Tag(SPIN)%
  ros::spin();
// %EndTag(SPIN)%

  return 0;
}
// %EndTag(FULLTEXT)%

/*
// TLV class test
int main () {

  vector<unsigned char> output;// = new vector<char>(7);
  TlvStr* mission = new TlvStr();
  mission->tag = 0x2;
  mission->str = "Hello this string is longer than 256 characters. The quick brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog. Hello this string is longer than 256 characters. The quick brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog.";
  mission->output(output);
  
  /TlvInt* int16item = new TlvInt();
  int16item->tag = 0x16;
  int16item->value = 0x1020;
  int16item->len = 2;
  
  int16item->output(output);
  
  TlvInt* int32item = new TlvInt();
  int32item->tag = 0x32;
  int32item->value = 0x10203040;
  int32item->len = 4;
  
  int32item->output(output);
  
/
  
  
  //cout << hex;
  for(int i = 0; i < output.size(); i++)
  {
  	printf("%X ", output[i]);
  } 
  cout << endl;
  
  return 0;
}

*/
