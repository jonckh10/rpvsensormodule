#include <iostream>
#include <sstream>
#include <stdio.h>
#include "tlv.h"

using namespace std;

	
void Tlv::berLen(vector<unsigned char>& out, int len)
{
   if(len <= 127)
   {
      uint8_t lenByte = len;
      out.push_back(lenByte);
   }
   else
   {		
      out.push_back(0x80);
      int lenIndex = out.size() - 1;
      //get past empty high order bytes			
      int offset = (sizeof(len) - 1) * 8;

      while(((len >> offset) & 0xFF) == 0)
      {
         offset -= 8;
      }
      while(offset >= 0)
      {
         char byte = len >> offset;
         out.push_back(byte);
         out[lenIndex]++;
         offset -= 8;
      }
   }
}


void TlvStr::output(vector<unsigned char>& out)
{
   out.push_back(tag);
   Tlv::berLen(out, str.size());
   //size-1 is to skip \0
   for(int i = 0; i < str.size(); i++)
   {
      out.push_back(str.at(i));
   }
}


void TlvInt::output(vector<unsigned char>& out)
{
   out.push_back(tag);
   out.push_back(len);
   for(int i = len-1; i >= 0; i--)
   {
      out.push_back((value >> i*8) & 0xFF);
   }
}

