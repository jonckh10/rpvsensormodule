#include <vector>
#include <inttypes.h>
#include <string>
#include <iostream>
#include <sstream>
#include <stdio.h>

using namespace std;

class Tlv
{
public:
	virtual void output(vector<unsigned char>& out) = 0;
	


	
	static void berLen(vector<unsigned char>& out, int len)
	{
		if(len <= 127)
		{
			uint8_t lenByte = len;
			out.push_back(lenByte);
		}
		else
		{		
			out.push_back(0x80);
			int lenIndex = out.size() - 1;
			//get past empty high order bytes			
			int offset = (sizeof(len) - 1) * 8;

			while(((len >> offset) & 0xFF) == 0)
			{
				offset -= 8;
			}
			while(offset >= 0)
			{
				char byte = len >> offset;
				out.push_back(byte);
				out[lenIndex]++;
				offset -= 8;
			}
		}
	}
};
class Tlv_str : public Tlv
{
public:
    char tag;
    std::string str;
	
	
	virtual void output(vector<unsigned char>& out)
	{
		out.push_back(tag);
		berLen(out, str.size());
		//size-1 is to skip \0
		for(int i = 0; i < str.size(); i++)
		{
			out.push_back(str.at(i));
		}
	}
};






class Tlv_int : public Tlv
{
public:
	char tag;
	char len;
	uint64_t value;

	void output(vector<unsigned char>& out)
	{
		out.push_back(tag);
		out.push_back(len);
		for(int i = len-1; i >= 0; i--)
		{
			out.push_back((value >> i*8) & 0xFF);
		}
	}


};

// TLV class test
int main () {

  vector<unsigned char> output;// = new vector<char>(7);
  Tlv_str* mission = new Tlv_str();
  mission->tag = 0x2;
  mission->str = "Hello this string is longer than 256 characters. The quick brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog. Hello this string is longer than 256 characters. The quick brown fox jumped over the lazy dog. The quick brown fox jumped over the lazy dog.";
  mission->output(output);
  
  /*Tlv_int* int16item = new Tlv_int();
  int16item->tag = 0x16;
  int16item->value = 0x1020;
  int16item->len = 2;
  
  int16item->output(output);
  
  Tlv_int* int32item = new Tlv_int();
  int32item->tag = 0x32;
  int32item->value = 0x10203040;
  int32item->len = 4;
  
  int32item->output(output);
  
*/
  
  
  //cout << hex;
  for(int i = 0; i < output.size(); i++)
  {
  	printf("%X ", output[i]);
  } 
  cout << endl;
  
  return 0;
}





class KlvFactory
{
private:
	int totalLen;
	vector<Tlv*> items;

	unsigned short bcc_16 (
	 unsigned char * buff, // Pointer to the first byte in the 16-byte UAS LDS key.
	 unsigned short len ) // Length from 16-byte UDS key up to 1-byte checksum length.
	{
		unsigned short bcc = 0, i; // Initialize Checksum and counter variables.
		for ( i = 0 ; i < len; i++)
			bcc += buff[i] << (8 * ((i + 1) % 2));
		return bcc;
	} // end of bcc_16 ()



public: 
	KlvFactory()
	{
		totalLen = 0;
	}
	
	void addInt(char tag, char len, uint64_t value)
	{
		Tlv_int *item = new Tlv_int;
		item->tag = tag;
		item->len = len;
		item->value = value;

		totalLen += len + 2;
		items.push_back(item);
	}
	
	void addStr(int tag, string s)
	{
			Tlv_str *item = new Tlv_str;
		item->tag = tag;
		item->str = s;
	
		//length of the beroid-encoded length field for the item
		int lenLen = s.size() < 127 ? 1 : 1 + s.size() / 256;
		totalLen += 1 + lenLen + s.size();
		items.push_back(item);	

	}

	//void addLds(char* lds, int len);
	
	vector<unsigned char> finish()
	{
		vector<unsigned char> out;
		
		char ulKey[] = { 0x06, 0x0E, 0x2B, 0x34, 0x02, 0x0B, 0x01, 0x01, 0x0E, 0x01, 0x03, 0x01, 0x01, 0x00, 0x00, 0x00 };
		
		//write ul key
		totalLen += sizeof(ulKey);
		for(int i = 0; i < sizeof(ulKey); i++)
		{
			out.push_back(ulKey[i]);
		}
		
		//write length field
		totalLen += 4; // add in space for checksum
		Tlv::berLen(out, totalLen);

		//write item values
		for(int i = 0; i < items.size(); i++)
		{
			items[i]->output(out);
		}

		//write checksum item
		out.push_back(1); //checksum tag
		out.push_back(2); //checksum length

		unsigned short checksum = bcc_16(out.data(), out.size());
		out.push_back(checksum & 0xFF);
		out.push_back(checksum >> 8);

		return out;
	}
};
/*
int main () {

  
  KlvFactory f;
  
  //f.addStr(5, "Hello");
	f.addInt(0x16, 2,  0x1020);	
	f.addInt(0x32, 4,  0x10203040);	
	  
  vector<unsigned char> output = f.finish();
  

  
  
  //cout << hex;
  for(int i = 0; i < output.size(); i++)
  {
  	printf("%X ", output[i]);
  } 
  cout << endl;
  
  return 0;
}
*/



/*
class ldsFactory
{
	public ldsFactory(ulKey);
	addTlv();
}

class Packet
{
	public Packet();
	
	
	
	private uint16_t mapToRange_16(float value, float high, float low)
	{
		if( value > high || value < low)
		{
			return 0x8000;
		}
		return (uint_16)round(0xFFFF/(high - low) * value);
	}
	
	private uint_32 mapToRange_32(float value, float high, float low)
	{
		if( value > high || value < low)
		{
			return 0x80000000;
		}
		return (uint_32)round(0xFFFFFFFE/(high - low) * value);
	}	
	
	setTimestamp(time);
	setPlatformAngle(heading, pitch, roll);
	setSensorLocation(latitude, longitude, altitude);
	setSensorRelAngle(azimuth, elevation, roll);
	setSlantRange(slantRange);
	setTargetWidth(targetWidth);
	setFrameCenterLocation(latitude, longitude, elevation);
	
	
	struct fastData
	{
		char ulKey[] = { 0x06, 0x0E, 0x2B, 0x34, 0x02, 0x0B, 0x01, 0x01, 0x0E, 0x01, 0x03, 0x01, 0x01, 0x00, 0x00, 0x00 } 
		const char length = 0x61;
		
		tlv_64 timestamp; //tag 2 is first REQ-3.01, contains birth of elements, probably synced to pps
		
		tlv_32 platformHeadingAngle;
		tlv_32 platformPitchAngle;
		tlv_32 platformRollAngle;
		
		tlv_32 sensorLatitude;
		tlv_32 sensorLongitude;
		tlv_16 sensorTrueAltitude;
		
		tlv_32 sensorRelAzAngle;
		tlv_32 sensorRelElAngle;
		tlv_32 sensorRelRollAngle;
		
		tlv_32 slantRange;
		tlv_32 targetWidth;
		
		tlv_32 frameCenterLatitude;
		tlv_32 frameCenterLongitude;
		tlv_16 frameCenterElevation;
		
		const char uasLdsVersion = {0x41, 0x01, 0x02};
		
		uint_16 checksum; //tag 1 is last REQ-3.03
	}
}
*/
