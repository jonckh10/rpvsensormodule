#include <vector>
#include <inttypes.h>
#include <string>
#include <iostream>
#include <sstream>
#include <stdio.h>

using namespace std;


class Tlv_int : public Tlv
{
public:
	char tag;
	char len;
	uint64_t value;

	void output(vector<unsigned char>& out)
	{
		out.push_back(tag);
		out.push_back(len);
		for(int i = len-1; i >= 0; i--)
		{
			out.push_back((value >> i*8) & 0xFF);
		}
	}


};
