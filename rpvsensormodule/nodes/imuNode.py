#!/usr/bin/env python
import roslib; roslib.load_manifest('rpvsensormodule')
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Imu 
from time import time
import math

'''
NOTICE: This file has not been tested.
'''

'''
This Robotic Operating System (ROS) node communicates with the Analog Devices 
ads16400 series Inertial Measurement Unit IMU driver, and outputs ROS Imu messages.

The IMU driver handles communication with the IMU and outputs the data to files
in the /sys/bus/iio/devices directory.
IMU settings are controlled by writing to files, but the node does not yet support this.
'''



'''
Base class containing the logic used to load and read the files associated with an IMU sensor
'''
class Sensor:
    unitScale = 0
    DEFAULT_PATH = '/sys/bus/iio/devices/iio:device0/'
    files = {}

    ''' initializes the sensor by opening all associated files
        Arguments:
            sensorName: name of the sensor as seen in the filename. Ex: accel, gyro
    '''
    def init(sensorName,path ):
        for axis in ['x', 'y', 'z']:
            files[axis] = {}
            for field in ['raw', 'scale', 'calibbias']:
                rawFile = File.open( path + '/in_' + name + '_' + axis +'_' + field)

    ''' reads the files associated with a sensor, and computes the x, y, and z readings
        Returns:
            [xAxisReading, yAxisReading, zAxisReading]
    '''
    def read():
        retVal = []
        for axis in ['x', 'y', 'z']:
            a = files[axis]
            retVal[axis] = (a['axis'] * a['raw'] + a['calibbias']) * unitScale
        return retVal


''' subclass of Sensor representing an accelerometer
'''
class Accelerometer(Sensor):
    Sensor.unitScale = 0.05
    def init(sensorPath = Sensor.DEFAULT_PATH):
        base('accel', sensorPath)
    
 
''' subclass of Sensor representing a gyroscope
'''
class Gyroscope(Sensor):
    Sensor.unitScale = 3.333e-3
    def init(path = Sensor.DEFAULT_PATH):
        base('gyro', path)

'''
Central class handling communication with other ROS nodes and gathering the IMU data
'''
class ImuNode:
   gyro = None
   accel = None
   sleepTime = 1

   def __init__(self):
      gyro = Gyroscope()
      accel = Accelerometer()

      self.pub = rospy.Publisher('imu', Imu)
      rospy.init_node('Imu')

   def run(self): 
      while not rospy.is_shutdown():
         imuData = Imu()
        

         (secs, fractSecs) = math.modf(time())
         imuData.header.stamp.secs = int(secs)
         imuData.header.stamp.nsecs = int(fractSecs * 1e9)

         imuData.orientation[0] = -1 #IMU does not supply orientation estimate

         imuData.orientation_covariance = 9 * [0] #unknown covariances
         imuData.angular_velocity_covariance[0] = 9 * [0]
         imuData.linear_accleration_covariance[0] = 9 * [0] 

         accelReadings = accel.read()
         imuData.linear_acceleration.x = accelReadings[0]
         imuData.linear_acceleration.y = accelReadings[1]
         imuData.linear_acceleration.z = accelReadings[2]

         imuData.gyroReadings = gyro.read()
         imuData.angular_velocity.x = gyroReadings[0]
         imuData.angular_velocity.y = gyroReadings[1]
         imuData.angular_velocity.z = gyroReadings[2]


         rospy.loginfo(imuData)
         self.pub.publish(imuData)
         rospy.sleep(self.sleepTime)


if __name__ == '__main__':
   n = ImuNode()
   n.sleepTime = 1.0
   n.run()

"""
3-Axis Accelerometer related device files:
in_accel_x_calibbias
in_accel_x_raw
in_accel_x_scale
in_accel_y_calibbias
in_accel_y_raw
in_accel_y_scale
in_accel_z_calibbias
in_accel_z_raw
in_accel_z_scale

3-Axis Gyro related device files:
in_gyro_x_calibbias
in_gyro_x_raw
in_gyro_x_scale
in_gyro_y_calibbias
in_gyro_y_raw
in_gyro_y_scale
in_gyro_z_calibbias
in_gyro_z_raw
in_gyro_z_scale

Miscellaneous device files:
in_temp0_offset
in_temp0_raw
in_temp0_scale
in_voltage0_supply_raw
in_voltage0_supply_scale
in_voltage1_raw
in_voltage1_scale

name
reset
sampling_frequency
sampling_frequency_available
"""
