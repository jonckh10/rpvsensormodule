#!/usr/bin/env python
import roslib; roslib.load_manifest('rpvsensormodule')
import rospy
import serial
 
from std_msgs.msg import String

#ser = serial.Serial(0)
ser = serial.Serial('/dev/ttyO2', 115200, timeout=1)

def callback(data):
   rospy.loginfo(rospy.get_name()+"transmitting: "+ data.data)
   ser.write(data.data)
   ser.write("writing data to serial port")

def listener():
   rospy.init_node('serialOut', anonymous=True)
   rospy.Subscriber('klvOut', String, callback)
   rospy.spin()

if __name__ == '__main__':
    listener()

