#include <ros/ros.h>
// Change following lines to "gps_common" if you're using Cturtle
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/NavSatStatus.h>
using namespace sensor_msgs;

void callback(const NavSatFixConstPtr &fix) {
  if (fix->status.status == NavSatStatus::STATUS_NO_FIX) {
    std::cout << "Unable to get a fix on the location." << std::endl;
    return;
  }

  std::cout << "Current Latitude: " << fix->latitude << std::endl;
  std::cout << "Current Longitude: " << fix->longitude << std::endl;
  std::cout << "Current Altitude: "<< fix->altitude<<std::endl;
  std::cout << "\n" << std::endl;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "gps_subscriber");
  ros::NodeHandle nh;
  ros::Subscriber gps_sub = nh.subscribe("fix", 1, callback);

  ros::spin();

  return 0;
}


